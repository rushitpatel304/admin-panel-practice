<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controller\admin_post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'front\postController@home');
Route::get('post/{slug}' , 'front\postdetailsController@post');
Route::get('/about_us' , 'front\pageController@page');


Route::view('/admin/login' , '/admin/login');
Route::post('/admin/login_submit' , 'admin_auth@login_submit');

Route::group(['middleware'=>'admin_auth'],function(){ 

    Route::get('/admin/post/list' , 'admin_post@listing');
    Route::get('/admin/edit/{id}' , 'admin_post@edit');
    Route::post('/admin/edit/{id}' , 'admin_post@update');
    Route::view('/admin/post_add' , '/admin/post_add');
    Route::post('/admin/post_add' , 'admin_post@add_post')->name('admin_post.add_post');
    Route::post('/admin/Delete/{id}' , 'admin_post@delete');

    Route::get('/admin/page_list' , 'pagesController@pages');
    // Route::get('/admin/post/contect' , 'admin_post@contect');
    Route::view('/admin/page_add' , '/admin/pages/page_add');
    Route::post('/admin/page_add' , 'pagesController@add_page')->name('admin_page.add_page');
    Route::post('/admin/page_edit/{id}' , 'pagesController@edit');
    Route::post('/admin/page_update/{id}' , 'pagesController@update');
    Route::post('/admin/page_delete/{id}' , 'pagesController@delete');

    Route::get('/admin/contect/list' , 'contectController@listing');

});

Route::any('/admin/logout' , 'admin_auth@logout');
