<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminPost extends Model
{
    protected $guarded = [];
    protected $table='post';
    public $timestamps = false;
}
