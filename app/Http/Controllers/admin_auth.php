<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class admin_auth extends Controller
{
    public function login_submit(Request $request){
       
        $email=$request->input('email');
        $password=$request->input('password');

         $result= User::where( 
             ['email'=>$email , 
              'password' =>$password
             ])->get();
            
         if(isset($result[0]->id)){
            $request->session()->put('user_id' , $result[0]->id);
            return redirect('/admin/post/list');
         }else{
            $request->session()->flash('msg' , 'Please enter valid login details');
            return redirect('/admin/login');
         }
    }
    
    public function logout(Request $request){

        $request->session()->forget('user_id');
         return redirect('/admin/login');
    }
}
