<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pages;
class pagesController extends Controller
{
    public function add_page(Request $request){
        // dd($request);
        $request->validate([
            'slug'=>'required | unique:pages'
        ]);

        $result= new pages;
        $result->name = $request->name;
        $result->slug = $request->slug;
        $result->description = $request->description;
        $result->added_on = $request->added_on;
         $result->save();
         $request->session()->flash('msg' , 'Data Saved');
        return redirect('/admin/page_list');
    }

    public function pages(){

        $pages=pages::all();
        return view('admin.pages.pages_list' , compact('pages'));
    }

    public function edit($id){
        $page=pages::find($id);
        return view('admin.pages.page_edit' , compact('page'));
    }

    public function update(Request $request , $id){

        $result=pages::find($id);
        $result->name = $request->name;
        $result->slug = $request->slug;
        $result->description = $request->description;
        $result->added_on = $request->added_on;
         $result->save();
         $request->session()->flash('msg' , 'Data Update Successfully');

        return redirect('/admin/page_list');
    }

    public function delete($id){

        $result=pages::find($id);
        $result->delete();
       
        return redirect('/admin/page_list');

    }
}
