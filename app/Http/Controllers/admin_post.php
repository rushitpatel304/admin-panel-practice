<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminPost;


class admin_post extends Controller
{
    public function add_post(Request $request){

        $result= new AdminPost;
        $result->title = $request->title;
        $result->short_desc = $request->short_desc;
        $result->long_desc = $request->long_desc;
        $result->slug = $request->slug;
        
        $image=$request->file('image');
        $ext=$image->extension();
        $file=time(). '.' .$ext;
        $image->storeAs('/public/post' , $file);
        $result->image = $file;

        
        $result->date = $request->date;
         $result->save();
         $request->session()->flash('msg' , 'Data Saved');
        return redirect('/admin/post/list');
    }
    public function listing(){

        $data=AdminPost::all();
        return view('admin.post_list' , compact('data'));
    }
    public function delete($id){
        $result=AdminPost::find($id);
        $result->delete();
       
        return redirect('/admin/post/list');

    }
    public function edit($id){
        $post=AdminPost::find($id);
        // dd($post);
        return view('admin.post_edit' , compact('post'));
    }
    public function update(Request $request , $id){
        $result=AdminPost::find($id);
        $result->title = $request->title;
        $result->short_desc = $request->short_desc;
        $result->image = $request->image;
        $result->date = $request->date;
         $result->save();
         $request->session()->flash('msg' , 'Data Update Successfully');

        return redirect('/admin/post/list');
    }
    
   
}
