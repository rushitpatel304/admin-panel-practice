<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminPost;

class postdetailsController extends Controller
{
    public function post($slug){

        $result=AdminPost::where('slug',$slug)->first();

        return view('front.post_details' , compact('result'));
    }
}
