<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\pages;

class pageController extends Controller
{
    public function page(){
        
        $pages=pages::all();
        return view('front/home');
    }
}
