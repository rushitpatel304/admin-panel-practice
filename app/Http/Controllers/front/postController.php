<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminPost;

class postController extends Controller
{
    public function home(){
        $result=AdminPost::all();
        return view('front.home' , compact('result'));
    }
}
