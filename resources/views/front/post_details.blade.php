@extends('front.layout.master')

@section('page_title' , 'My Blog')
@section('post_details')

<header class="masthead" style="background-image: url('{{asset('storage/post/'.$result->image)}}')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <h3>{{$result->title}}</h3>
          <span class="subheading">{{$result->short_desc}}</span>
        </div>
      </div>
    </div>
  </div>
</header>
  <!-- Main Content -->
   
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          <p>{{$result->long_desc}}</P>
        </div>
        <hr>
      </div>
    </div>
  </div>     

@endsection
  