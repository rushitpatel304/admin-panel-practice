@extends('front.layout.master')
@section('page_title' , 'Home')

@section('home')

  
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('{{asset('front_asset/img/home-bg.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>
 
  <!-- Main Content -->
  
  <div class="container">
    <div class="row">
      @foreach ($result as $list)
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          <a href="{{url('post/'.$list->slug)}}">
            <h2 class="post-title">
            {{$list->title}}
            </h2>
          </a>
            <h3 class="font-weight-normal">
            {{$list->short_desc}}
            </h3>
        
          <p class="post-meta">Posted on {{$list->date}} </p>
        </div>
        <hr>
      </div>
      @endforeach
    </div>
  </div>    

@endsection
  