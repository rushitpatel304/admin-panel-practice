@extends('admin/layout.master')
@section('page_title' , 'Post Contect')
@section('post_contect')
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col  ">
            <div>
                <button type="button" class="btn btn-outline-secondary"><a href="/admin/post_add">Add Post</a></button>
            </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Post </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Title</th>
                          <th>Short Desc</th>
                          <th>Image</th>
                          <th>Date</th>
                          <th>Action</th>

                        </tr>
                      </thead>
                      <tbody>
                     
                      </tbody>
                    </table>
                    {{session('msg')}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection