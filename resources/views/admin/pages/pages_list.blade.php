@extends('admin/layout.master')
@section('page_title' , ' Pages List')
@section('pages_list')
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col  ">
            <div>
                <button type="button" class="btn btn-info"><a href="{{url('/admin/page_add')}}">Add Page</a></button>
            </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Pages </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Name</th>
                          <th>Slug</th>
                          <th class="w-25">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                       @foreach ($pages as $page)
                       <tr>
                         <td>{{$page->id}}</td>
                         <td>{{$page->name}}</td>
                         <td>{{$page->slug}}</td>
                         <td class="d-flex" >
                          <form method="post" action="/admin/page_edit/{{$page->id}}">
                            @csrf
                            <button type="submit" class="btn btn-success">Edit</button>
                          </form>
                          <form method="POST" action="/admin/page_delete/{{$page->id}}">
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                          </form>
                        </td>
                       </tr>
                       @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection