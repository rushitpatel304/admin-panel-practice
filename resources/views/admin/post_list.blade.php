@extends('admin/layout.master')
@section('page_title' , 'Post Listing')
@section('post_list')
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col  ">
            <div>
                <button type="button" class="btn btn-info  text-#ffffff"><a class="text-#ffffff" href="/admin/post_add">Add Post</a></button>
            </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Post </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Title</th>
                          <th>Short Desc</th>
                          <th class="w-25">Image</th>
                          <th>Date</th>
                          <th>Action</th>

                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($data as $list) 
                        <tr>
                          <th>{{$list->id}}</th>
                          <td>{{$list->title}}</td>
                          <td>{{$list->short_desc}}</td>
                          <td><img class="w-25" src="{{asset('storage/post/'.$list->image)}}"></td>
                          <td>{{$list->date}}</td> 
                          <td class="d-flex">
                          <form method="GET" action="/admin/edit/{{$list->id}}">
                            @csrf
                            <button type="submit" class="btn btn-success">Edit</button>
                          </form>
                          <form method="POST" action="/admin/Delete/{{$list->id}}">
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                          </form>
                        </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{session('msg')}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection