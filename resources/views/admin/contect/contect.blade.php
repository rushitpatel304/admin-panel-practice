@extends('admin/layout.master')
@section('page_title' , 'Contect')
@section('contect')
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row" style="display: block;">
              <div class="col  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contect </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Mobile</th>
                          <th>Message</th>
                          <th>Added On</th>
                        </tr>
                      </thead>
                      <tbody>
                       @foreach ($contects as $contect)
                       <tr>
                         <td>{{$contect->id}}</td>
                         <td>{{$contect->name}}</td>
                         <td>{{$contect->email}}</td>
                         <td>{{$contect->mobile}}</td>
                         <td>{{$contect->message}}</td>
                         <td>{{$contect->added_on}}</td>
                       </tr>
                       @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection