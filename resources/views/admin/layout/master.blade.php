<!DOCTYPE html>
<html lang="en">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>@yield('page_title')</title>

	<!-- Bootstrap -->
	<link href="{{ asset('admin_asset/css/bootstrap.min.css')}}" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{{asset('admin_asset/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
	<!-- NProgress -->
	<link href="{{ asset('admin_asset/css/nprogress.css')}}" rel="stylesheet">
	<!-- iCheck -->
	<link href="{{asset('admin_asset/css/green.css')}}" rel="stylesheet">
	<!-- bootstrap-wysiwyg -->
	<link href="{{asset('admin_asset/css/prettify.min.css')}}" rel="stylesheet">
	<!-- Select2 -->
	<link href="{{asset('admin_asset/css/select2.min.css')}}" rel="stylesheet">
	<!-- Switchery -->
	<link href="{{asset('admin_asset/css/switchery.min.css')}}" rel="stylesheet">
	<!-- starrr -->
	<link href="{{asset('admin_asset/css/starrr.css')}}" rel="stylesheet">
	<!-- bootstrap-daterangepicker -->
	<link href="{{asset('admin_asset/css/daterangepicker.css')}}" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="{{asset('admin_asset/css/custom.min.css')}}" rel="stylesheet">
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="/" class="site_title"><i class="fa fa-paw"></i> <span>My Blog</span></a>
					</div>

					<div class="clearfix"></div>
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<li><a><i class="fa fa-home"></i> Home </a>
								
								</li>
								<li><a href="/admin/post_add"><i class="fa fa-edit"></i> Add Post </a>

								</li>
								<li><a href="/admin/post/list"><i class="fa fa-table"></i>List</a>
								</li>
								<li><a href="/admin/page_list"> <i class="fa fa-desktop"></i> Pages</a>
                                </li>
                                <li><a href="/admin/contect/list"> <i class="fa fa-desktop"></i> Contect Us</a>
                                </li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<nav class="nav navbar-nav">
						<ul class=" navbar-right">
							<li class="nav-item dropdown open" style="padding-left: 15px;">
								<a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
									Admin
								</a>
								<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
								
									<a class="dropdown-item" href={{url('/admin/logout')}}><i class="fa fa-sign-out pull-right"></i> Log Out</a>
								</div>
							</li>

						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
		@yield('post_add')
		@yield('post_list')
		@yield('post_edit')
		@yield('pages_list')
		@yield('post_contect')
		@yield('add_page')
		@yield('edit_page')
		@yield('contect')
			<!-- footer content -->
			<footer>
				<div class="pull-right">
				
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>

	<!-- jQuery -->
	<script src="{{asset('admin_asset/js/jquery.min.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('admin_asset/js/bootstrap.bundle.min.js')}}"></script>
	<!-- FastClick -->
	<script src="{{asset('admin_asset/js/fastclick.js')}}"></script>
	<!-- NProgress -->
	<script src="{{asset('admin_asset/js/nprogress.js')}}"></script>
	<!-- bootstrap-progressbar -->
	<script src="{{asset('admin_asset/js/bootstrap-progressbar.min.js')}}"></script>
	<!-- iCheck -->
	<script src="{{asset('admin_asset/js/icheck.min.js')}}"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="{{asset('admin_asset/js/moment.min.js')}}"></script>
	<script src="{{asset('admin_asset/js/daterangepicker.js')}}"></script>
	<!-- bootstrap-wysiwyg -->
	<script src="{{asset('admin_asset/js/bootstrap-wysiwyg.min.js')}}"></script>
	<script src="{{asset('admin_asset/js/jquery.hotkeys.js')}}"></script>
	<script src="{{asset('admin_asset/js/prettify.js')}}"></script>
	<!-- jQuery Tags Input -->
	<script src="{{asset('admin_asset/js/jquery.tagsinput.js')}}"></script>
	<!-- Switchery -->
	<script src="{{asset('admin_asset/js/switchery.min.js')}}"></script>
	<!-- Select2 -->
	<script src="{{asset('admin_asset/js/select2.full.min.js')}}"></script>
	<!-- Parsley -->
	<script src="{{asset('admin_asset/js/parsley.min.js')}}"></script>
	<!-- Autosize -->
	<script src="{{asset('admin_asset/js/autosize.min.js')}}"></script>
	<!-- jQuery autocomplete -->
	<script src="{{asset('admin_asset/js/jquery.autocomplete.min.js')}}"></script>
	<!-- starrr -->
	<script src="{{asset('admin_asset/js/starrr.js')}}"></script>
	<!-- Custom Theme Scripts -->
	<script src="{{asset('admin_asset/js/custom.min.js')}}"></script>

</body></html>