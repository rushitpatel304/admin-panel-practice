@extends('admin/layout.master')
@section('page_title' , 'Manage Post')

@section('post_edit')

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Post</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
        
                    
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align"  for="title">Title<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" id="title" required="required" name="title" class="form-control " value="{{$post->title}}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align"  for="slug" >Slug<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" id="slug" required="required" name="slug" class="form-control" value="{{$post->slug}}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="short_desc">Short Desc<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <textarea type="text" id="short_desc"  name="short_desc" required="required" class="form-control" >{{$post->short_desc}}</textarea>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="long_desc">Long Desc<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <textarea type="text" id="long_desc"  name="long_desc" required="required" class="form-control">{{$post->long_desc}}</textarea>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align"  for="image">Image<span class="required" ></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="file" id="image" required="required" name="image" value="{{$post->image}}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Post Date<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input id="post_date" class="date-picker form-control" name="date" value="{{$post->date}}" placeholder="dd-mm-yyyy" type="text" required="required" type="text" onfocus="this.type='date'" onmouseover="this.type='date'" onclick="this.type='date'" onblur="this.type='text'" onmouseout="timeFunctionLong(this)">
                                    <script>
                                        function timeFunctionLong(input) {
                                            setTimeout(function() {
                                                input.type = 'text';
                                            }, 60000);
                                        }
                                    </script>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 offset-md-3">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

</div>
@endsection